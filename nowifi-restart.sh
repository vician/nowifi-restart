#!/bin/bash

route=$(ip r | grep default | awk '{print $3}')

if [ -z "$route" ]; then
	echo "Cannot find route ip!"
	exit 1
fi

default=$(ip route get $route | head -n 1 | awk '{print $1}')
if [ -z "$default" ]; then
	echo "Cannot find default ip!"
	exit 1
fi

if [ "$default" != "$route" ]; then
	echo "Unexpected error: route ip isn't the same as default ip!"
	exit 1
fi

echo "route: $route"

ping -c 1 $route
if [ $? -eq 0 ]; then
	echo "Router is reachable"
	exit 0
fi

sudo ifdown wlan0
sudo ifup wlan0
